package com.example

import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import kotlin.test.*
import io.ktor.server.testing.*
import com.example.plugins.*
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.statement.*

class ApplicationTest {

    @Test
    fun testGet() {
        val url = "https://api.thecatapi.com/v1/images/search"
        assertNotNull(url)
        }

}